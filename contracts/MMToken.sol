//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MMToken is ERC20 {
    constructor() ERC20("Marius Muk Token", "MMT") {
        _mint(msg.sender, 100000 * (10 ** 18));
    }
}