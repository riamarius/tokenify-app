// import logo from './logo.svg';
import { useState } from "react"
import { ethers } from "ethers"
import "./App.css"
import Greeter from "./artifacts/contracts/Greeter.sol/Greeter.json"
import Token from "./artifacts/contracts/Token.sol/Token.json"

// Update with the contract address logged out to the CLI when it was deployed
const greeterAddress = "0x5FbDB2315678afecb367f032d93F642f64180aa3"
const tokenAddress = "0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512"
//const tokenAddress = "0x0B306BF915C4d645ff596e518fAf3F9669b97016"

function App() {
  // store greeting in local state
  const [greeting, setGreetingValue] = useState("")
  const [greetingResult, setGreetingResult] = useState("result comes here")
  const [userAccount, setUserAccount] = useState("")
  const [amount, setAmount] = useState()
  const [balance, setBalance] = useState(0)

  // request access to the user's MetaMask account
  async function requestAccount() {
    await window.ethereum.request({ method: "eth_requestAccounts" })
  }

  // call the smart contract, read the current greeting value
  async function fetchGreeting() {
    if (typeof window.ethereum !== "undefined") {
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(
        greeterAddress,
        Greeter.abi,
        provider
      )
      try {
        const data = await contract.greet()
        setGreetingResult(data)
        console.log("data: ", data)
      } catch (err) {
        console.log("Error: ", err)
      }
    } else {
      alert("Please connect your wallet")
    }
  }

  // call the smart contract, send an update
  async function setGreeting() {
    if (!greeting) return
    if (typeof window.ethereum !== "undefined") {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const signer = provider.getSigner()
      const contract = new ethers.Contract(greeterAddress, Greeter.abi, signer)
      const transaction = await contract.setGreeting(greeting)
      await transaction.wait()
      fetchGreeting()
      setGreetingValue("")
    } else {
      alert("Please connect your wallet")
    }
  }

  async function getBalance() {
    if (typeof window.ethereum !== "undefined") {
      const [account] = await window.ethereum.request({
        method: "eth_requestAccounts",
      })
      console.log("====================================")
      console.log(account)
      console.log("====================================")
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const contract = new ethers.Contract(tokenAddress, Token.abi, provider)
      const balance = await contract.balanceOf(account)
      const symbol = await contract.symbol()
      const name = await contract.name()
      setBalance(`${balance.toString()} ${symbol} (${name})`)
      console.log("Balance: ", symbol)
    }
  }

  async function sendCoins() {
    if (typeof window.ethereum !== "undefined") {
      await requestAccount()
      const provider = new ethers.providers.Web3Provider(window.ethereum)
      const signer = provider.getSigner()
      const contract = new ethers.Contract(tokenAddress, Token.abi, signer)
      const transaction = await contract.transfer(userAccount, amount)
      await transaction.wait()
      console.log(`${amount} Coins successfully sent to ${userAccount}`)
      // contract.transfer(userAccount, amount).then(data => console.log({ data }))
      getBalance()
      setUserAccount("")
      setAmount("")
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <button onClick={fetchGreeting}>Fetch Greeting</button>
        <span>{greetingResult}</span>
        <br />
        <button onClick={setGreeting}>Set Greeting</button>
        <input
          onChange={(e) => setGreetingValue(e.target.value)}
          value={greeting}
        />
        <br />
        <button onClick={getBalance}>Get Balance</button>
        <span>{balance}</span>
        <br />
        <button onClick={sendCoins}>Send Coins</button>

        <input
          onChange={(e) => setUserAccount(e.target.value)}
          placeholder="Account ID"
          value={userAccount}
        />
        <br />
        <input
          onChange={(e) => setAmount(e.target.value)}
          placeholder="Amount"
          value={amount}
        />
      </header>
    </div>
  )
}

export default App
